/*
 * 
 */
package edu.westga.cs1302.bugcensus.model;

import java.util.ArrayList;
import java.util.Calendar;

import edu.westga.cs1302.bugcensus.resources.UI;

/**
 * The Class BugData.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class BugCensus {
	private int year;
	private ArrayList<Bug> bugs;
	private int numberInsects;
	private int numberMyriapodas;
	
	/**
	 * Instantiates a new bug census.
	 * 
	 * @precondition none
	 * @postcondition getYear() == currYear && getBugs().size == 0 && getNumberInsects == 0 && getNnumberMyriapodas == 0
	 */
	public BugCensus() {
		Calendar now = Calendar.getInstance();
		this.year = now.get(Calendar.YEAR);
		this.bugs = new ArrayList<Bug>();
		this.numberInsects = 0;
		this.numberMyriapodas = 0;
	}
	
	/**
	 * Instantiates a new bug census.
	 * 
	 * @precondition year > 0
	 * @postcondition getYear() == year && getBugs().size == 0 && getNumberInsects == 0 && getNnumberMyriapodas == 0
	 * @param year the year
	 */
	public BugCensus(int year) {
		this.year = year;
		this.bugs = new ArrayList<Bug>();
		this.numberInsects = 0;
		this.numberMyriapodas = 0;
	}

	/**
	 * Gets the year.
	 *
	 * @return the year
	 */
	public int getYear() {
		return this.year;
	}

	/**
	 * Gets the bugs.
	 *
	 * @return the bugs
	 */
	public ArrayList<Bug> getBugs() {
		return this.bugs;
	}

	/**
	 * Gets the number insects.
	 *
	 * @return the number insects
	 */
	public int getNumberInsects() {
		return this.numberInsects;
	}

	/**
	 * Gets the number myriapodas.
	 *
	 * @return the number myriapodas
	 */
	public int getNumberMyriapodas() {
		return this.numberMyriapodas;
	}
	
	/**
	 * Size.
	 *
	 * @return the size of the bugs array list
	 */
	public int size() {
		return this.bugs.size();
	}
	
	/**
	 * Adds the bug
	 * If insect or myriapoda, increment count
	 * 
	 * @precondition bug != null
	 * @postcondition size(prev) == size() -1
	 * @param bug the bug
	 * @return true, if successful
	 */
	public boolean add(Bug bug) {
		if (bug == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BUG);
		}
		
		this.bugs.add(bug);
		
		if (bug.getClass() == Insect.class) {
			this.numberInsects++;
		}
		if (bug.getClass() == Bee.class) {
			this.numberInsects++;
		}
		if (bug.getClass() == Myriapoda.class) {
			this.numberMyriapodas++;
		}
		
		return true;
	}
	
	/**
	 * Gets the summary report.
	 *
	 * @return the summary report
	 */
	public String getSummaryReport() {
		String output = "";
		output += "Bug Census of " + this.year + System.lineSeparator();
		output += "Total number bugs: " + this.size() + System.lineSeparator();
		output += "Number insects: " + this.numberInsects + System.lineSeparator();
		output += "Total myriapodas: " + this.numberMyriapodas + System.lineSeparator();	
		return output;
	}
	
}
