package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;

/**
 * The Enum BeeCaste.
 * 
 * @author Darrell Branch
 */
public enum BeeCaste {
	
	/** The worker. */
	WORKER, 
	/** The drone. */
	DRONE, 
	/** The queen. */
	QUEEN;
	
	/**
	 * Parses the caste.
	 *
	 * @precondition caste !- null and BeeCaste.values contains caste
	 * @postcondition none
	 *
	 * @param caste the caste
	 * @return the bee caste
	 */
	public static BeeCaste parseCaste(String caste) {
		if (caste == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BEECASTE);
		}
		
		caste = caste.toUpperCase();
		
		switch (caste) {
			case "WORKER":
				return WORKER;
			case "DRONE":
				return DRONE;
			case "QUEEN":
				return QUEEN;
			default:
				throw new IllegalArgumentException("Invalid caste");
		}
	}

}
