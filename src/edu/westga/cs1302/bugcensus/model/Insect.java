package edu.westga.cs1302.bugcensus.model;

import javafx.scene.paint.Color;

/**
 * The Class Insect.
 * 
 * @author Darrell Branch
 */
public class Insect extends Bug {
	
	/** The winged. */
	private boolean winged;
	
	/**
	 * Instantiates a new insect.
	 *
	 * @param length the length
	 * @param winged the winged
	 * @param color the color
	 * @precondition length > 0 && color != null
	 * @postcondition getLength() == length && getNumberLegs() == 6 && getColor() == color && isWinged() == winged
	 */
	public Insect(double length, boolean winged, Color color) {
		super(length, 6, color);
		
		this.winged = winged;
	}
	
	/**
	 * Checks if is winged.
	 *
	 * @return true, if is winged
	 */
	public boolean isWinged() {
		return this.winged;
	}

	@Override
	public String toString() {
		return "Insect length= " + this.getLength() + " winged=" + this.winged + " color=" + this.getColor();
	}
	
}
