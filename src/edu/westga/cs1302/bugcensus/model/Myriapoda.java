package edu.westga.cs1302.bugcensus.model;

import javafx.scene.paint.Color;

/**
 * The Class Myriapoda.
 * 
 * @author Darrell Branch
 */
public class Myriapoda extends Bug {
	private int numberSegments;
	
	/**
	 * Instantiates a new myriapoda.
	 * 
	 * @precondition length > 0 && numberLegs >= 0 && color != null && numberSegments >= 0
	 * @postcondition getLength() == length && getNumberLegs() == numberLegs && getColor = color && getNumberSegments == numberSegments
	 * @param length the length
	 * @param numberLegs the number legs
	 * @param numberSegments the number segments
	 * @param color the color
	 */
	public Myriapoda(double length, int numberLegs, int numberSegments, Color color) {
		super(length, numberLegs, color);
		
		this.numberSegments = numberSegments;
	}

	/**
	 * Gets the number segments.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return the number segments
	 */
	public int getNumberSegments() {
		return this.numberSegments;
	}

	@Override
	public String toString() {
		return "Myriapoda length=" + this.getLength() + " #legs=" + this.getNumberLegs() + " #segments=" + this.numberSegments + " color=" + this.getColor();
	}

}
