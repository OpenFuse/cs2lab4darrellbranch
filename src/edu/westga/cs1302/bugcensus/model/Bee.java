package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.paint.Color;

/**
 * The Class Bee.
 * 
 * @author Darrell Branch
 */
public class Bee extends Insect {
	
	/** The caste. */
	private BeeCaste caste;
	
	/**
	 * Instantiates a new bee.
	 *
	 * @param length the length
	 * @param caste the caste
	 * @precondition length > 0 && caste != null
	 * @postcondition getLength() == length && getNumberLegs() == 6 && getColor() == Color.BLACK && isWinged() == true && getCaste() == caste
	 */
	public Bee(double length, BeeCaste caste) {
		super(length, true, Color.BLACK);
		
		this.caste = caste;
	}

	/**
	 * Gets the caste.
	 *
	 * @return the caste
	 */
	public BeeCaste getCaste() {
		return this.caste;
	}

	/**
	 * Sets the caste.
	 * 
	 * @precondition caste != null
	 * @postcondition getCaste == caste
	 * @param caste the new caste
	 */
	public void setCaste(BeeCaste caste) {
		if (caste == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BEECASTE);
		}
		
		this.caste = caste;
	}

	@Override
	public String toString() {
		return "Bee length=" + this.getLength() + " caste=" + this.caste;
	}
	
}
