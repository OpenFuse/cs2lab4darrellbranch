package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.paint.Color;

/**
 * The Class Bug.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class Bug {
	
	/** The length. */
	private double length;
	
	/** The number legs. */
	private int numberLegs;
	
	/** The color. */
	private Color color;
	
	/**
	 * Instantiates a new bug.
	 *
	 * @param length the length
	 * @param numberLegs the number legs
	 * @param color the color
	 * @precondition length > 0 && numberLegs >= 0 && color != null
	 * @postcondition getLength() == length && getNumberLegs() == numberLegs && getColor = color
	 */
	public Bug(double length, int numberLegs, Color color) {
		if (length <= 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NONPOSITIVE_LENGTH);
		}
		if (numberLegs < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_NUMBER_LEGS);
		}
		if (color == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_COLOR);
		}
		
		this.length = length;
		this.numberLegs = numberLegs;
		this.color = color;
	}

	/**
	 * Gets the length.
	 *
	 * @return the length
	 */
	public double getLength() {
		return this.length;
	}

	/**
	 * Gets the number legs.
	 *
	 * @return the number legs
	 */
	public int getNumberLegs() {
		return this.numberLegs;
	}

	/**
	 * Gets the color.
	 *
	 * @return the color
	 */
	public Color getColor() {
		return this.color;
	}

	@Override
	public String toString() {
		return "Bug length=" + length + " #legs=" + numberLegs + " color=" + color;
	}
	
}
