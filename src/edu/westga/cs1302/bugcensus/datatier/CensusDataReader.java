package edu.westga.cs1302.bugcensus.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

import edu.westga.cs1302.bugcensus.model.Bee;
import edu.westga.cs1302.bugcensus.model.BeeCaste;
import edu.westga.cs1302.bugcensus.model.Bug;
import edu.westga.cs1302.bugcensus.model.BugType;
import edu.westga.cs1302.bugcensus.model.Insect;
import edu.westga.cs1302.bugcensus.model.Myriapoda;
import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.paint.Color;

/**
 * The Class CensusDataReader - reads words to be used in the word cloud from a
 * text file.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class CensusDataReader {

	private File censusFile;
	
	/**
	 * Instantiates a new census data file reader
	 *
	 * @precondition censusFile != null
	 * @postcondition none
	 * 
	 * @param censusFile
	 *            the file to read the census data from
	 */
	public CensusDataReader(File censusFile) {
		if (censusFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_CENSUS_FILE);
		}
		
		this.censusFile = censusFile;
	}

	/**
	 * Opens the associated file and reads the census year.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the census year
	 */
	public int loadCensusYear() {
		int year = 0;
		try (Scanner censusScanner = new Scanner(this.censusFile)) {
			String line = censusScanner.nextLine();
			year = Integer.parseInt(line);
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} catch (NoSuchElementException e) {
			System.err.println("Empty census file");
			System.err.println(e.getMessage());
		} catch (Exception e) {
			System.err.println("First line does not contain a census year");
			System.err.println(e.getMessage());
		}
		return year;
	}
	
	/**
	 * Opens the associated file and reads all the bug data stored in the file.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return Collection of bugs read from the file.
	 */
	public ArrayList<Bug> loadCensusData() {
		ArrayList<Bug> bugs = new ArrayList<Bug>();
		
		try (Scanner censusScanner = new Scanner(this.censusFile)) {
			censusScanner.nextLine();
			while (censusScanner.hasNextLine()) {
				String line = censusScanner.nextLine();
				Bug bug = this.createBug(line);
				try {
					bugs.add(bug);
				} catch (Exception e) {
					System.err.println("Bug could not be created");
					System.err.println(e.getMessage());
				}
			}	
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} catch (NoSuchElementException e) {
			System.err.println("Empty census file");
			System.err.println(e.getMessage());
		} 
		
		return bugs;
	}
	
	/**
	 * Creates a bug from the given string
	 * 
	 * @param line the string containing the data of a bug
	 * @return the bug that has been created, null if the line contained invalid data 
	 */
	private Bug createBug(String line) {
		Bug bug = null;
		try {
			String[] fields = line.split(",");
			String type = fields[0];
			double length = Double.parseDouble(fields[1]);
			int numberLegs = Integer.parseInt(fields[2]);
			if (type.equalsIgnoreCase(BugType.BUG.toString())) {
				Color color = Color.web(fields[4]);
				bug = new Bug(length, numberLegs, color);
			} else if (type.equalsIgnoreCase(BugType.MYRIAPODA.toString())) {
				int numberSegments = Integer.parseInt(fields[3]);
				Color color = Color.web(fields[4]);
				bug = new Myriapoda(length, numberLegs, numberSegments, color);
			} else if (type.equalsIgnoreCase(BugType.INSECT.toString())) {
				Color color = Color.web(fields[4]);
				boolean winged = Boolean.parseBoolean(fields[5]);
				bug = new Insect(length, winged, color);
			} else if (type.equalsIgnoreCase(BugType.BEE.toString())) {
				BeeCaste caste = BeeCaste.parseCaste(fields[6]);
				bug = new Bee(length, caste);
			} 
		} catch (Exception e) {
			System.err.println("Line with invalid data");
			System.err.println(e.getMessage());
		}
		return bug;
	}
}
